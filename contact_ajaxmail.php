<?php
$nm=$_POST['username'];
$em=$_POST['useremail'];
$ph=$_POST['userphone'];
$cmp=$_POST['usercompany'];
$msg=$_POST['usermessage'];

if(trim($nm)!="Your Name" && trim($em)!="Your Email" && trim($ph)!="Your Phone" && trim($cmp)!="Your Company"&& trim($msg)!="Your Message" &&
 trim($nm)!="" && trim($em)!="" && trim($ph)!="" && trim($cmp)!="" && trim($msg)!="")
{
	if(filter_var($em, FILTER_VALIDATE_EMAIL))
	{
		?>
		<style>
			@import url('https://fonts.googleapis.com/css2?family=Varela+Round&display=swap');
			p{
				font-family: Varela round;
			}
		</style>
		<script >
			//grecaptcha.execute();
		</script>
		<?php
		require 'vendor/autoload.php';
		$API_KEY = "API_KEY";

		$useremail = new \SendGrid\Mail\Mail();
		$useremail->setFrom("example@gmail.com");
		$useremail->setSubject("Thanks for contact ");
		$useremail->addTo($em);
		$useremail->addContent(
			"text/html", "
			<div style='font-family: Varela round;background-color: #F5F5F5; padding: 60px; margin-bottom: 40px; border-bottom: 3px solid #eaeaea;'>
			<p style='font-size: 16px; margin-bottom:20px;'>Hi $nm,</p>
			
			<p style='font-size: 16px; margin-bottom:20px;'>Thanks for reaching out to Team . One of our business partner team member will reach out to you . Explore our Digital solutions here</p>
			
			<p style='font-size: 16px;margin-bottom:-15px;margin-top:40px;'>With best regards,</p>
			<p style='font-size: 16px;'></p>
			</div>
			"
		);
		$sendgrid = new \SendGrid($API_KEY);
		


		$companyemail = new \SendGrid\Mail\Mail();
		$companyemail->setFrom("example@gmail.com");
		$companyemail->setSubject("New user");
		$companyemail->addTo("example@gmail.com");
		$companyemail->addContent(
			"text/html", "
			<div style='font-family: Varela round;background-color: #F5F5F5; padding: 60px; margin-bottom: 40px; border-bottom: 3px solid #eaeaea;'>
			<p style='font-size: 16px; margin-bottom:20px;'>Hey Team..</p>

			<p style='font-size: 16px; margin-bottom:40px;'>Below is the communication from one of the existing or prospective customer. It is our utmost importance to serve the customer on a timely basis. Please reach out to your superior with this communication and respond to the customer with in 24hours or immediately.</p>
			

			<p style='font-size: 16px; margin-bottom:10px;'><strong>Name : </strong> $nm</p>

			<p style='font-size: 16px; margin-bottom:10px;'><strong>Email : </strong> $em</p>
			
			<p style='font-size: 16px; margin-bottom:10px;'><strong>Phone : </strong> $ph</p>

			<p style='font-size: 16px; margin-bottom:10px;'><strong>Company : </strong> $cmp</p>

			<p style='font-size: 16px; margin-bottom:10px;'><strong>Message : </strong> $msg</p>

			<p style='font-size: 16px;margin-bottom:-15px;margin-top:40px;'>With best regards,</p>
			<p style='font-size: 16px;'>Team </p>
			</div>
			"

		);
		$sendgrid = new \SendGrid($API_KEY);




		if($sendgrid->send($useremail))
		{
		$sendgrid->send($companyemail);
			
		echo '1#<span style="color:green;">Mail has been sent successfully.</span>';
		}
		else
		{
		echo '4#Please, Try Again.';
		}
	}
	else
		echo '3#Please, provide valid Email.';
}
else
{
echo '2#Please, fill all the details.';
} 
?>