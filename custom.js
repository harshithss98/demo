jQuery(window).load(function () {
  "use strict";
  jQuery("#status").fadeOut();
  jQuery("#preloader").delay(50).fadeOut("slow");
});

$(document).ready(function () {
  // one page
  $.fn.single = function (options) {
    var opts = $.extend({}, $.fn.single.defaults, options);
    return this.each(function () {
      var element = $(this);
      changeCSS(element);
      changeIMG(opts);
      $(window).bind("resize", function () {
        changeCSS(element);
        changeIMG(opts);
      });
      $("[data-link]").bind("click", function (event) {
        event.preventDefault();
        goToSection(this, opts);
      });
    });
  };

  // contact mail function start
  $("#cont_msg").keypress(function (e) {
    var key = e.which;
    if (key == 13) {
      // the enter key code
      $("#contact_form_submit").click();
      return false;
    }
  });

  $(
    ".app_form_wrapper .apps_input input, .app_form_wrapper .apps_input textarea"
  ).keypress(function () {
    $(this).parent().removeClass("error");
  });
// contact mail function end

function sendMail() {
  var co_name = $("#cont_name").val();
  var co_email = $("#cont_email").val();
  var co_phone = $("#cont_phone").val();
  var co_company = $("#cont_company").val();
  var co_message = $("#cont_msg").val();
  let data = {
    username: co_name,
    useremail: co_email,
    userphone: co_phone,
    usercompany: co_company,
    usermessage: co_message,
  };
  $("#loading").show();
  $("#contact_form_submit").hide();
  $.ajax({
    type: "POST",
    url: "contact_ajaxmail.php",
    data: data,
    success: function (contact) {
      //grecaptcha.execute();
      $("#loading").hide();
      $("#contact_form_submit").show();

      var i = contact.split("#");
      if (i[0] == "1") {
        $("#cont_name").val("");
        $("#cont_email").val("");
        $("#cont_phone").val("");
        $("#cont_company").val("");
        $("#cont_msg").val("");
        $("#contact_err").html(i[1]);

        $(".app_form_wrapper .apps_input").addClass("success");
        setTimeout(function () {
          $(".app_form_wrapper .apps_input").removeClass("success");
          $(".app_form_wrapper .apps_input").removeClass("error");
          $("#cont_email").parent().removeClass("error");
        }, 2500);
      } else {
        $("#cont_name").val(data.username);
        $("#cont_email").val(data.useremail);
        $("#cont_phone").val(data.userphone);
        $("#cont_company").val(data.usercompany);
        $("#cont_msg").val(data.usermessage);
        $("#contact_err").html(i[1]);

        $(
          ".app_form_wrapper .apps_input input, .app_form_wrapper .apps_input textarea"
        ).each(function () {
          if (!$(this).val()) {
            $(this).parent().addClass("error");
          } else {
            if (i[0] == 3) {
              $("#cont_email").parent().addClass("error");
            } else {
              $(this).parent().addClass("error");
            }
            $(this).parent().removeClass("error");
          }
        });
      }
    },
  });
}